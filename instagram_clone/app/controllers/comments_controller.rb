class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :destroy]
  
  def new
    @comment = Picture.find_by(id: params[:id]).build
  end

  def show
    @picture = Picture.find_by(id: params[:id])
    @comments = @picture.comments
    @user = @picture.user
  end
  
  def create
    @comment = Picture.find_by(id: params[:id]).build
  end

  private
    def comment_params
      params.require(:comment).permit(:user_id, :content)
    end
end
