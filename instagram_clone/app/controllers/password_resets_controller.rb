class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :logged_in_user, only: [:edit, :update]

  def new
  end

  def edit
  end

  def update
    if params[:user][:new_password].empty?
      flash[:danger] = "New password can't be empty!"
      render 'edit'
      return
    end

    if @user && @user.authenticate(params[:user][:password])
      if params[:user][:new_password] == params[:user][:password_confirmation]
        params[:user][:password] = params[:user][:new_password]
        @user.update_attributes(user_params)
        flash[:success] = "Password changed."
        redirect_to user_url(@user)
      else
        flash[:danger] = "new password and cofirmation are differente."
        render 'edit'
      end
    else
      flash[:danger] = "Current password is wrong."
      render 'edit'
    end
  end

  private
    def user_params
      params.require(:user).permit(:password)
    end

    #before_action
    def get_user
      @user = User.find(params[:id])
    end
end
