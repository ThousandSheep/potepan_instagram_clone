class PicturesController < ApplicationController
  before_action :logged_in_user, only: [:create, :show, :destroy]
  before_action :correct_user,   only: :destroy

  def new
    @picture = current_user.pictures.build
  end

  def create
    @picture = current_user.pictures.build(picture_params)
    if @picture.save
      flash[:success] = "Picture uploaded!"
    else
      flash[:danger] = "There are something wrong with the picture!"
    end
    redirect_to request.referrer || root_url
  end

  def show
    @picture = Picture.find(params[:id])
  end

  def update
  end

  def destroy
    #[TODO] 削除したファイルがディレクトリに残り続けている
    @picture.remove_picture!
    @picture.save
    @picture.destroy
    flash[:success] = "The picture deleted"
    redirect_to request.referrer || root_url
  end

  private
    #before_action
    def picture_params
      params.require(:picture).permit(:picture)
    end

    def correct_user
      @picture = current_user.pictures.find_by(id: params[:id])
      redirect_to root_url if @picture.nil?
    end
end
