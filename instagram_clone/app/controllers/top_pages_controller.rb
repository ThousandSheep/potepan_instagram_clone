class TopPagesController < ApplicationController
  def top_page
    if logged_in?
      @picture         = current_user.pictures.build
      @time_line_items = current_user.time_line.paginate(page: params[:page])
    else
      @time_line_items = []
    end
  end
end
