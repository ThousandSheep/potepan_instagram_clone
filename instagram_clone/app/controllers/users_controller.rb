class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]

  def new
    @user = User.new
  end

  def index
    @users = User.where.not(id: current_user.id ).paginate(page:params[:page])
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in(@user)
      flash[:success] = "Welcome!"
      redirect_to user_url(@user)
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "updated!"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def show
    @user = User.find(params[:id])
    @pictures = @user.pictures.paginate(page: params[:page])
  end

  def destroy
    User.find(params[:id]).destroy
    log_out
    flash[:success] = "Thank you for using our service."
    redirect_to root_url
  end

  private
    def user_params
      params.require(:user).permit(:first_name, :last_name, :user_name,
                                   :email,
                                   :web_site, :self_introduce,
                                   :phone, :gender,
                                   :password,
                                   :password_confirmation)
    end

    # 正しいユーザーかどうか確認
    def correct_user
      @user = User.find(params[:id])
      unless current_user?(@user)
        flash[:danger] = "You have accessed a wrong path."
        redirect_to(root_path)
      end
    end
end
