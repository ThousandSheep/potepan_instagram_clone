class Comment < ApplicationRecord
  belongs_to :picture
  default_scope -> {order(created_at: :desc)}
  validates :user_id, presence: true
  validates :content, presence: true, length: {maximum: 250}
end
