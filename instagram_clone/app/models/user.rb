class User < ApplicationRecord
  has_many :pictures, dependent: :destroy
  has_many :active_relationships, class_name: "Relationship",
                                 foreign_key: "follower_id",
                                   dependent: :destroy
  has_many :passive_relationships,class_name: "Relationship",
                                 foreign_key: "followed_id",
                                   dependent: :destroy
  has_many :following, through: :active_relationships,  source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  before_save {self.email = email.downcase}
  #[TODO]URLエンコードがしたい
  #before_save {CGI.escape(self.web_site)}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  #[TODO]下記の正規表現ではPhone is invalid とエラーが出るが、解消方法がわからない
  #VALID_PHONE_REGEX = /\d+/
  validates :first_name, presence: true, length:{maximum:20}
  validates :last_name,  presence: true, length:{maximum:20}
  validates :user_name,  presence: true, length:{maximum:40}
  validates :email,      presence: true, length:{maximum:100},
                         format:{with:VALID_EMAIL_REGEX},
                         uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password,       presence: true, length: {minimum: 6}, allow_nil: true
  validates :self_introduce, length: {maximum: 500}
  #validates :phone,          format:{with:VALID_PHONE_REGEX}

  # 渡された文字列のハッシュ値を返す
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def follow(other_user)
    following << other_user
  end

  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 渡されたユーザがログインしているユーザをフォローしていればtrue
  def following?(other_user)
    following.include?(other_user)
  end

  # ログイン中のユーザ自身とフォローしているユーザのpictureを返す
  def time_line
    following_ids = "SELECT followed_id FROM relationships WHERE
                     follower_id = :user_id"
    Picture.where("user_id IN (#{following_ids})
                   OR user_id = :user_id", user_id: id)
  end
end
