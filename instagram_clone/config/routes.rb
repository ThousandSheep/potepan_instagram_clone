Rails.application.routes.draw do
  get 'password_resets/new'
  get 'password_resets/edit'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root   'top_pages#top_page'
  get    '/signup',  to: 'users#new'
  post   '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :pictures,        only: [:new, :create, :show, :update, :destroy]
  resources :relationships,   only: [:create, :destroy]
  resources :comments
end
