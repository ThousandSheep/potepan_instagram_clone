class AddAdditionalUserInfoToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :web_site, :string
    add_column :users, :self_introduce, :text
    add_column :users, :phone, :string
    add_column :users, :gender, :boolean
  end
end
