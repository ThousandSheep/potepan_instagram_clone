class AddIndexToPicturesUserId < ActiveRecord::Migration[5.2]
  def change
    add_index :pictures, [:user_id, :created_at]
  end
end
