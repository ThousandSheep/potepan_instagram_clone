class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :content
      t.references :picture, foreign_key: true

      t.timestamps
    end
    add_index :comments, [:picture_id, :created_at]
  end
end
