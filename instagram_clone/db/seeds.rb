# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# user
User.create!(first_name: "QianXiang",
             last_name: "Qin",
             user_name: "Mila",
             self_introduce: "Fluctuat nec mergitur", 
             email: "qin@example.com",
             password:              "password",
             password_confirmation: "password")

99.times do |n|
  first_name  = Faker::Name.first_name
  last_name  = Faker::Name.last_name
  user_name = first_name
  self_introduce = Faker::Lorem.sentence(10)
  email = "example-#{n+1}@example.com"
  password = "password"
  User.create!(first_name: first_name,
               last_name: last_name,
               user_name: user_name,
               self_introduce: self_introduce,
               email: email,
               password:              password,
               password_confirmation: password)
end

# picture
users = User.order(:created_at).take(4)
7.times do
  num = rand(1..8)
  users.each { |user| user.pictures.create!(picture: open("#{Rails.root}/db/picture_dev/picture-#{num}.png")) }
end

# relationship
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

#comment
pictures = Picture.all
20.times do
  num = rand(1..6)
  user = User.find_by(id: num)
  dummy_text = Faker::Lorem.sentence(5)
  pictures.each { |picture| picture.comments.create!(content: dummy_text, user_id: user.id) }
end