require 'test_helper'

class PicturesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @picture = pictures(:starfish)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Picture.count' do
      post pictures_path, params: {picture: {picture: "starfish.png"}}
    end
    assert_redirected_to login_path
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Picture.count' do
      delete picture_path(@picture)
    end
    assert_redirected_to login_path
  end
end
