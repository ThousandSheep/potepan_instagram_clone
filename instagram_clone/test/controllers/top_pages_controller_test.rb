require 'test_helper'

class TopPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title ="instagram_clone"
  end
  
  test "should get TopPage" do
    get root_url
    assert_response :success
    assert_select "title", "TopPage | #{@base_title}"
  end

end
