require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:qin)
  end

  test "change password requires log in" do
    get edit_password_reset_path(@user)
    assert_redirected_to login_path
    assert_not flash.empty?
  end

  test "new password can't be empty" do
    log_in_as(@user)
    patch password_reset_path(@user), params: {user: {new_password: ""}}
    assert_template 'password_resets/edit'
    assert_not flash.empty?
  end

  test "new password and the confirmaion have to be same" do
    get edit_password_reset_path(@user) #friendly forwarding test
    log_in_as(@user)
    patch password_reset_path(@user), params: {user: {password: "password",
                                                  new_password: "password1",
                                         password_confirmation: "password2"}}
    assert_template 'password_resets/edit'
    assert_not flash.empty?
  end

  test "change password succeed" do
    log_in_as(@user)
    patch password_reset_path(@user), params: {user: {password: "password",
                                                  new_password: "password1",
                                         password_confirmation: "password1"}}
    assert_redirected_to user_url(@user)
    assert_not flash.empty?
    delete logout_path
    post login_path, params: {session: {email:@user.email, password: "password1"}}
    assert_redirected_to user_url(@user)
  end
end
