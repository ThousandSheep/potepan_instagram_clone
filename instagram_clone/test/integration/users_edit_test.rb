require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:qin)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: {user: {first_name: "",
                                            last_name: "",
                                            user_name: "",
                                            email: "" }}
    assert_template 'users/edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_user_path(@user) #friendly forwarding test
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    first_name  = "Foo"
    last_name = "Bar"
    user_name = "HogeHoge"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: {first_name: first_name,
                                             last_name: last_name,
                                             user_name: user_name,
                                             email: email }}
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal first_name,  @user.first_name
    assert_equal last_name,  @user.last_name
    assert_equal user_name,  @user.user_name
    assert_equal email, @user.email
  end
end
