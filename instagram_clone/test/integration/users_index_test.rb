require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:qin)
  end

  test "index including pagination" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    # [TODO] User.paginate(page:1) に@userが存在しているらしい。
    # マイページだとfirst_name表示なので、text:user.user_name がエラーになる
    # User.paginate(page:1).each do |user|
    #   assert_select 'a[href=?]', user_path(user), text:user.user_name
    # end
  end

  test "login user should not display on user index page" do
    log_in_as(@user)
    get users_path
    assert_no_match @user.user_name, @response.body
  end
end
