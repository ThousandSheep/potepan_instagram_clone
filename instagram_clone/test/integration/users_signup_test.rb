require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: {user: {first_name: "",
                                      last_name: "",
                                      user_name: "",
                                      email: "user@invalid",
                                      password: "foo",
                                      password_confrimation: "bar",}}
    end
    assert_template "users/new"
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: {user: {first_name: "QianXiang",
                                      last_name: "Qin",
                                      user_name: "Mila",
                                      email: "qin@example.com",
                                      password: "qianxiang",
                                      password_confrimation: "qianxiang",}}
    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
  end
end
