require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  def setup
    @user = users(:qin)
    @picture = pictures(:starfish)
    @comment = @picture.comments.build(user_id: @user.id, content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @comment.valid?
  end

  test "user id should be present" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test "content can't be too long" do
    @comment.content = "a" * 251
    assert_not @comment.valid?
  end

  test "order should be most recent first" do
    assert_equal comments(:most_recent), Comment.first
  end

  test "associated comments should be destroyed" do
    @picture.save
    @picture.comments.create!(user_id: @user.id, content: "Lorem ipsum")
    assert_difference 'Comment.count', -1 do
      @picture.destroy
    end
  end
end
