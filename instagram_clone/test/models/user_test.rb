require 'test_helper'
include ActionDispatch::TestProcess

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(first_name:"QianXiang", last_name:"Qin", 
                     user_name:"Mila", email:"qin@sample.com",
                     password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "first_name can't be empty" do
    @user.first_name = ""
    assert_not @user.valid?
  end

  test "last_name can't be empty" do
    @user.last_name = ""
    assert_not @user.valid?
  end

  test "user_name can't be empty" do
    @user.user_name = ""
    assert_not @user.valid?
  end

  test "email can't be empty" do
    @user.email = ""
    assert_not @user.valid?
  end

  test "first_name should not be too long" do
    @user.first_name = "a" * 21
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 101
    assert_not @user.valid?
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  # [TODO] URL encode
  # test "web_site should be encoded" do
  #   url_with_char_should_encode = "https://qianxiang.com/index.html&q=あああ"
  #   @user.web_site = url_with_char_should_encode
  #   @user.save
  #   assert_equal CGI.escape(url_with_char_should_encode), @user.reload.web_site
  # end

  #[TODO] @user.pictures.build(picture: "kraken.png")時、作成されたpictureはnilになる。
  # その結果、{:picture=>["can't be blank"]}が出る
  # test "associated pictures should be destroyed with user" do
  #   @user.save
  #   debugger
  #   @user.pictures.build(picture: "kraken.png")
  #   debugger
  #   assert_difference 'Picture.count', -1 do
  #     @user.destroy
  #   end
  # end

  test "should follow and unfollow a user" do
    qin = users(:qin)
    archer  = users(:archer)
    assert_not qin.following?(archer)
    qin.follow(archer)

    assert qin.following?(archer)
    assert archer.followers.include?(qin)

    qin.unfollow(archer)
    assert_not qin.following?(archer)
  end

  test "feed should have the right posts" do
    qin    = users(:qin)
    archer = users(:archer)
    lana   = users(:lana)
    # フォローしているユーザーの投稿を確認
    lana.pictures.each do |post_following|
      assert qin.time_line.include?(post_following)
    end
    # 自分自身の投稿を確認
    qin.pictures.each do |post_self|
      assert qin.time_line.include?(post_self)
    end
    # フォローしていないユーザーの投稿を確認
    archer.pictures.each do |post_unfollowing|
      assert_not qin.time_line.include?(post_unfollowing)
    end
  end
end
